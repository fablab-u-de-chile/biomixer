# BioMixer

[![CC BY 4.0][cc-by-sa-shield]][cc-by-sa]

La Biomixer es una máquina de Control Numérico que permite dispensar, mezclar y calentar ingredientes con precisión para cocinar bioplásticos. Su interfaz es un servicio web que calcula y ejecuta recetas a partir de porcentajes y cantidades. La Biomixer es el corazón del Nodo, ya que es el punto de partida para los demás procesos; con ella se vinculan la [bioformadora](https://gitlab.com/fablab-u-de-chile/termoformadora), la bio [rotomoldeadora](https://gitlab.com/fablab-u-de-chile/rotomoldeadora) y la Impresora 3D de biomateriales.

Este proyecto es parte de la iniciativa [Nodo de BioFabricación Digital](https://gitlab.com/fablab-u-de-chile/NBD) liderada por el FabLab U. de Chile, financiado por el Ministerio de Culturas, Artes y Patrimonio de Chile, apoyado por la plataforma internacional [Materiom](https://materiom.org/), y desarrollado en colaboración con estudiantes e investigadores de la Universidad de Chile.

<img src="/img/biomixer1.jpg" height="300"> <img src="/img/biomixer2.jpg" height="300">

NOTA: La máquina no incluye el sistema de cocina y mezcla de la sustancia, por ahora se complementa con un agitador magnético como [este](https://www.ubuy.cl/en/product/OQH554A-sh-2-magnetic-stirrer-hot-plate-dual-controls-heating-stirring-holder-laboratory).

<img src="/img/bioiso.png" width="600">

## Atributos

- [Dispensado de polvos finos](https://gitlab.com/fablab-u-de-chile/dispensador-de-polvo-horizontal)
- [Dispensado de polvos con alto roce](https://gitlab.com/fablab-u-de-chile/dispensador-de-polvo-v1)
- [Dispensado de fluidos viscosos](https://gitlab.com/fablab-u-de-chile/bomba-peristaltica)
- Dispensado de líquidos no viscosos.
- Alertas sonoras y de luz.
- Sensado de temperatura de la mezcla.
- Chasis de perfiles de aluminio V-Slot.
- Arduino Mega como controlador y [Biomixer Shield]() dedicado para control de dispensadores.
- [Interfaz de usuario](https://gitlab.com/fablab-u-de-chile/biomixer-web-interface) en servidor Web, montado en Raspberry.

En este [enlace](https://youtu.be/rvIczn-h5E8) puedes ver el modelo anterior de la Biomixer funcionando.

## Cómo construirla

- Listado de [partes, piezas y herramientas](docs/parts.md).
- [Partes CAD](parts/).
- Sistema [electrónico](docs/elec.md).
- Instrucciones de [ensamble](docs/assembly.md).
- Montar el [servidor Web](https://gitlab.com/fablab-u-de-chile/biomixer-web-interface).

## Trabajo futuro

Estamos trabajando para que a corto plazo el software permita almacenar y compartir recetas. En una segunda etapa, esperamos que la data pueda estar vinculada a la plataforma Materiom con el propósito de compartir información con biofabricadores de todo el mundo. A futuro se espera que la máquina sea capaz de variar las fórmulas de acuerdo a propiedades de materiales que solicite el usuario, gracias a la aplicación de redes neuronales.


- Agregar al repositorio instrucciones de ensamble y cableado.

### Contáctanos!

- [Página Web](http://www.fablab.uchile.cl/)
- [Instagram](https://www.instagram.com/fablabudechile/?hl=es-la)
- [Youtube](https://www.youtube.com/channel/UC4pvq8aijaqn5aN02GiDwFA)

# Licencia
[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

Este trabajo esta publicado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY%20SA%204.0-lightgrey.svg
