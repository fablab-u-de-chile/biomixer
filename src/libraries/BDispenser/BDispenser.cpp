#include <BDispenser.h>
#include <Arduino.h>

BDispenser::BDispenser(int pd1, int pd2, int ppwm)
{
pinMode(pd1,OUTPUT);
pinMode(pd2,OUTPUT);
pinMode(ppwm,OUTPUT);
_pd1=pd1;
_pd2=pd2;
_ppwm=ppwm;
_Kp=1.0;
_ready=true;
_type="none";
_material="none";
_density=1.0;
}

BDispenser::~BDispenser()
{
    //dtor
}

String BDispenser::bmaterial()
{
	return _material;
}

String BDispenser::btype(){
	return _type;
}

void BDispenser::assign(String type, String material, float density){
	_type=type;
	_material=material;
	_density=density;
}

void BDispenser::setkp(float Kp){
	_Kp=Kp;
}

void BDispenser::dispense(float setmass, float readmass){
	float trumass = setmass*_density;
	float error=trumass-readmass;
	float pwr=_Kp*error;

	pwr=constrain(pwr,_MIN_PWM,_MAX_PWM);
	pwr=int(pwr);
	_ready=readmass>=trumass;

	digitalWrite(_pd1,HIGH);
	digitalWrite(_pd2,LOW);
	analogWrite(_ppwm,pwr);

}

void BDispenser::retraction(){
	digitalWrite(_pd1,LOW);
	digitalWrite(_pd2,HIGH);
	analogWrite(_ppwm,255);
}

void BDispenser::turnoff(){
	digitalWrite(_pd1,LOW);
	digitalWrite(_pd2,LOW);
	digitalWrite(_ppwm,0);
}

bool BDispenser::isReady(){
	return _ready;
}
