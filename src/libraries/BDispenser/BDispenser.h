#ifndef BDispenser_h_density
#define BDispenser_h

#include <Arduino.h>

#define _MIN_PWM 100
#define _MAX_PWM 255

class BDispenser
{
    public:
    BDispenser(int pd1, int pd2, int ppwm);
    ~BDispenser();
    int pin();
    void dispense(float setmass, float readmass);
    void assign(String type, String material, float density);
    void retraction();
    String bmaterial();
    String btype();
    bool isReady();
    void turnoff();
    void setkp(float Kp);
    private:
      int _pd1;
      int _pd2;
      int _ppwm;
      float _Kp;
      bool _ready;
      String _type;
      String _material;
      float _density;
};

#endif // BDispenser_H
