# Partes y piezas

Esta máquina puede usar distintas combinaciones de dispensadores, para poder construirlos, dirígete al repositorio de cada uno de ellos.

- [Dispensador de polvo fino](https://gitlab.com/fablab-u-de-chile/dispensador-de-polvo-horizontal)
- [Dispensador de polvos de alto roce](https://gitlab.com/fablab-u-de-chile/dispensador-de-polvo-v1)
- [Bomba peristáltica para fluidos viscosos](https://gitlab.com/fablab-u-de-chile/bomba-peristaltica)

La configuración que hemos utilizado hasta ahora es de 2 dispensadores de polvo fino, uno de polvo de alto roce, y una bomba peristáltica.

El quinto dispensador que utilizamos es prácticamente una componente comercial: [una bomba de agua modelo R385](https://rambal.com/bomba-valvula-solenoide/694-bomba-de-diafragma-6-12v-dc-r385.html).

Los sistemas que mueven líquidos tienen reservas de material independiente del actuador, el detalle de como ensamblarlos se muestra en la guía de ensamble.

## Partes comerciales

Las partes comerciales aparecen detalladas en esta [planilla](parts/BIOMIXERCOMPARTS.xlsx) separadas por subensamble, sin considerar los dispensadores y la electrónica.

## Partes impresas

Los archivos .step y .stl los puedes encontrar en la [carpeta de partes](parts/). En esta [planilla](parts/BIOMIXER3DPPARTS.xlsx) se muestra a que subensamble pertenece.


## Herramientas

 ITEM |                  
 --------------------------- |  
 Sierra |
 Llave allen 4 mm |
 Llave allen 3 mm |
 Llave allen 2 mm |
 Llave allen 1.5 mm |
 Silicona para sellar |




