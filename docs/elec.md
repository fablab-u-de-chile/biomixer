# Electrónica

El sistema electrónico de la Biomixer consiste en un Arduino Mega con un Biomixer Shield dedicado que controla los motores de los dispensadores y lee los sensores de temperatura y peso, además cuenta con una Raspberry Pi donde está alojado el servidor de la interfaz de usuario donde se ingresan y almacenan las mezclas y recetas de biomateriales, y se dan las ordenes al Arduino Mega para que prepare las recetas.

![diagrama biomixer](img/biomix-elec.png "Diagrama electrónico Biomixer")

La cantidad de puentes H dependerá de cuantos dispensadores se incorporarán en la máquina, cada puente H soporta dos dispensadores. En la versión actual de la Biomixer se pueden controlar 6 dispensadores de materiales.


## Listado de componentes

 ITEM              | Cantidad     | Referencia
 ----------------- | ------------ | ----  |
Raspberry Pi 4 | 1
Módulo de alimentación de Raspberry | 1
Cable USB A-B | 1
Arduino MEGA | 1
Puente H L298N | 3
Celda de carga 1 kg | 1
Amplificador HX711 | 1
Módulo termocupla MAX6675 | 1
Fuente de poder 12 V 5A | 1
Módulo buzzer | 1
Módulo LED RGB | 1
Biomixer shield   | 1
Step Down module LM2596S  | 1   | [Enlace altronics](https://altronics.cl/modulo-stepdown-ajustable-lm2596s?search=step%20down)



## Biomixer Shield

![capa superior](img/biomix-pcb-top.png "capa superior pcb")

![capa inferior](img/biomix-pcb-bot.png "capa inferior pcb")

![esquematico](img/biomix-sch.png "esquemático pcb")
